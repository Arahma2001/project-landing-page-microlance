module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        'body': ['Roboto', 'sans-serif'],
      },
      backgroundImage: theme => ({
        'main-background': "url('asset/img/Main_Background.png')",
      }),
      colors: {
        'main-color-blue': 'linear-gradient(#553BE6, #A87FFA)',
        'title-color-blue-1': '#232F55',
        'title-color-blue-2': '#2A3A60',
        'body-text-color-gray-1': '#88879E',
        'card-color-blue-1': '#1fb6fc',
        'card-color-blue-2': '#eff2ff',
      },
      screens: {
        'hp': '360px',
      },
      boxShadow: {
        'blue-100': '0 15px 25px 0 rgba(0, 4, 74, .04)',
        'black-200': '0 20px 48px -4px rgba(0, 0, 0, .09)',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
