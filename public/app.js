const nav = document.querySelector('#main-nav');
const logo = document.querySelector('#logo');
const logoText = document.querySelector('#logo-text');
const registerButton = document.querySelector('#button-register');

// on scroll action
window.addEventListener('scroll', (e) => {
    if(window.scrollY >= 40) {
        nav.classList.add('bg-white');
        nav.classList.add('shadow-sm');

        logo.src = "asset/img/Logo_Microlance.png";
        logoText.classList.add('text-blue-600');

        registerButton.classList.add('border-blue-600');
        registerButton.classList.add('hover:bg-blue-600');

    } else {
        nav.classList.remove('bg-white');
        nav.classList.remove('shadow-sm');

        logo.src = "asset/img/Logo_Microlance_White.svg";
        logoText.classList.remove('text-blue-600');

        registerButton.classList.remove('border-blue-600');
        registerButton.classList.remove('hover:bg-blue-600');
    }
})


// Carousel
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})

var owl = $('.owl-carousel');
owl.owlCarousel();
// Go to the next item
$('.customNextBtn').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.customPrevBtn').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
})